void initTimer0() {
	
	TIMSK |= (1 << TOIE0);
	TCCR0 |= (1<<CS01); // Prescaler 8 (CPU clock / 8)
}

void initTimer2() {

	TIMSK |= (1 << OCIE2); //Timer2 Compare Match Interrupt enable
	ASSR  |= (1 << AS2); //is clocked from crystal Oscillator
	OCR2 = 128; // at 128 overflow
	TCCR2 |= (1 << WGM21);	// uses OCR2
	TCCR2 |= (1 << CS22) | (1 << CS21); //Prescaler 256, 32.768hz / 256 = 128 -> 128 = 1sec
}

void initButton() {
	
	GICR |= (1 << INT0); //enable INT0
	MCUCR |= (1 << ISC01); //Interrupt on falling edge
	setK2; 
}




