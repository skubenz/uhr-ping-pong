#define F_CPU 8000000UL // set CPU clk to 8mhz

#include <stdbool.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "PortDef.h"
#include "Output.h"
#include "Initializing.h"

volatile bool push = false;
volatile int sleep;
volatile int sec = 50;
int min = 59;
int hr = 11;

void time() { 

	if(sec == 60) {
		min++;
		sec = 0;	
	}
	if(min == 60) {
		hr++;
		min = 0;
	}
	if(hr == 12) hr = 0;
}

void print() {

	for(int column = 0; column < 12; column++){
		if(column == 0)shift(false);
		else shift(true);
				
			for(int line = 0; line < 10 ; line++){
				if(show_display(hr, min, sec, line, column)) set_line(line);
				else reset_line(line);
				
				//brighter LED�s
				for(volatile int i = 100; i>0; i--) {
				} 
						
				reset_line(line);
			}
	}
}

//------------------------------------------------------------------------------
int main (){

	initPorts (); 
	initTimer0();	
	initTimer2();
	initButton();
	sei(); //interrupts global enable
	setSTR; 
	clearShiftRegister(); //activate Shift Registers
	sleep_enable();
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sleep_mode();


	while(1) {
 	if(push && sec == sleep) { //delay doesn�t work
			push = false;
			sleep_mode();
	 	}	
	}
}
//---------------------------------------------------------------------

ISR(TIMER0_OVF_vect) {

	if(push) print();	
}

ISR(TIMER2_COMP_vect) {
	
	sec++;
	time();
}

ISR(INT0_vect) { 
	
	sleep_disable();
	push = true;
	sleep = sec + 30;
	if(sleep >= 60) sleep -= 60;
	sleep_enable();
}



