#include <avr/io.h>

//initialize Ports
void initPorts() {

	//lines
	DDRC |= (1 << PC0); //entweder so, alles auf eine zeile oder hexa
	DDRC |= (1 << PC1);
	DDRC |= (1 << PC2);
	DDRC |= (1 << PC3);
	DDRD |= (1 << PD4);
	DDRD |= (1 << PD5);
	DDRD |= (1 << PD6);
	DDRD |= (1 << PD7);
	DDRB |= (1 << PB0);
	DDRB |= (1 << PB1);

	//Shift Register / columns
	DDRB |= (1 << PB2);
	DDRB |= (1 << PB3);
	DDRB |= (1 << PB4);

	//Button
	DDRD |= (1 << PD2);
}

//lines
#define setC0 PORTC |= (1 << PC0); 
#define setC1 PORTC |= (1 << PC1);
#define setC2 PORTC |= (1 << PC2);
#define setC3 PORTC |= (1 << PC3);

#define setD4 PORTD |= (1 << PD4);
#define setD5 PORTD |= (1 << PD5);
#define setD6 PORTD |= (1 << PD6);
#define setD7 PORTD |= (1 << PD7);

#define setB0 PORTB |= (1 << PB0);
#define setB1 PORTB |= (1 << PB1);

#define resetC0 PORTC &= ~(1 << PC0); 
#define resetC1 PORTC &= ~(1 << PC1);
#define resetC2 PORTC &= ~(1 << PC2);
#define resetC3 PORTC &= ~(1 << PC3);

#define resetD4 PORTD &= ~(1 << PD4);
#define resetD5 PORTD &= ~(1 << PD5);
#define resetD6 PORTD &= ~(1 << PD6);
#define resetD7 PORTD &= ~(1 << PD7);

#define resetB0 PORTB &= ~(1 << PB0);
#define resetB1 PORTB &= ~(1 << PB1);

//Shift Register / columns 
#define setSTR PORTB |= (1 << PB2);
#define setD PORTB |= (1 << PB4);
#define setCLK PORTB |= (1 << PB3);

#define resetSTR PORTB &= ~(1 << PB2);
#define resetD PORTB &= ~(1 << PB4);
#define resetCLK PORTB &= ~(1 << PB3);

//Button
#define setK2 PORTD |= (1 << PD2); 
#define resetK2 PORTD &= ~(1 << PD2); 

void set_line(int y) {
	
	switch(y) {
		case 0: setC0; break;
		case 1: setC1; break;
		case 2: setC2; break;
		case 3: setC3; break;
		case 4: setD4; break;
		case 5: setD5; break;
		case 6: setD6; break;
		case 7: setD7; break;
		case 8: setB0; break;
		case 9: setB1; break;
	}
}

void reset_line(int y) {

	switch(y) {
		case 0: resetC0; break;
		case 1: resetC1; break;
		case 2: resetC2; break;
		case 3: resetC3; break;
		case 4: resetD4; break;
		case 5: resetD5; break;
		case 6: resetD6; break;
		case 7: resetD7; break;
		case 8: resetB0; break;
		case 9: resetB1; break;
	}	
}

void reset_all_lines() {

	resetC0;
	resetC1;
	resetC2;
	resetC3;
	resetD4;
	resetD5;
	resetD6;
	resetD7;
	resetB0;
	resetB1;
}

void shift(bool x) {
	
	if(x) {
		setD;
	} else resetD;
	//rising edge generate
	resetCLK;
	setCLK;
}

void clearShiftRegister() {
	
	for(int i = 0; i <= 11; i++) {
		shift(true);
	}
}




